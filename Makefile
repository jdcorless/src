#
# makefile for src
#

prefix?=/usr/local
mandir?=/share/man
target=$(DESTDIR)$(prefix)

VERS=$(shell ./src version | sed -n -e "/src: /s///p")

SOURCES = README INSTALL COPYING NEWS TODO src srctest tapview src.adoc FAQ.adoc Makefile control

TAPCONSUMER=./tapview

all: src.1

# To see the raw TAP output, run "make TAPCONSUMER=cat check"
check: pylint
	make pylint
	./srctest | $(TAPCONSUMER)

.SUFFIXES: .html .adoc .1

# Requires asciidoc and xsltproc/docbook stylesheets.
.adoc.1:
	a2x --doctype manpage --format manpage $<
.adoc.html:
	a2x --doctype manpage --format xhtml -D . $<
	rm -f docbook-xsl.css

FAQ.html: FAQ.adoc
	asciidoc -a toc FAQ.adoc

clean:
	rm -f  *~ *.1 *.html *.tar.gz MANIFEST
	rm -fr .rs* typescript test/typescript

src-$(VERS).tar.gz: $(SOURCES) src.1
	@ls $(SOURCES) src.1 | sed s:^:src-$(VERS)/: >MANIFEST
	@(cd ..; ln -s src src-$(VERS))
	(cd ..; tar -czf src/src-$(VERS).tar.gz `cat src/MANIFEST`)
	@(cd ..; rm src-$(VERS))

COMMON_PYLINT = --rcfile=/dev/null --reports=n \
	--msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" \
	--dummy-variables-rgx='^_'
PYLINTOPTS = "C0103,C0123,C0301,C0302,C0325,C0326,C0330,C0410,C0413,C1001,C0111,R0101,R0902,R0903,R0904,R0201,R0912,R0913,R0914,R0915,R1705,R1723,R1724,W0110,W0141,W0142,W0143,W0232,W0311,W0312,W0603,W0511,W0613,E0401,E1101,E1103"
pylint:
	@pylint $(COMMON_PYLINT) --disable=$(PYLINTOPTS) src

version:
	@echo $(VERS)

dist: src-$(VERS).tar.gz

release: src-$(VERS).tar.gz src.html FAQ.html
	shipper version=$(VERS) | sh -e -x

refresh: src.html FAQ.html
	shipper -N -w version=$(VERS) | sh -e -x

install: all
	install -d "$(target)/bin"
	install -d "$(target)$(mandir)/man1"
	install src "$(target)/bin"
	install -m644 src.1 "$(target)$(mandir)/man1"
